export const STORAGE_KEY_CONFIG = "start-config"

export function getConfix() {
    const config = localStorage.getItem(STORAGE_KEY_CONFIG)
    if (!config) {
        return null
    }
    return JSON.parse(config);
}

export function setConfig(config) {
    localStorage.setItem(STORAGE_KEY_CONFIG, JSON.stringify(config))
}