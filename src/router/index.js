import Vue from 'vue'
import VueRouter from 'vue-router'
import Start from '../views/Start.vue'
import Questions from '../views/Questions.vue'
// import Results from '../views/Results.vue'
// import { CategoriesAPI } from "@/components/CategoriesAPI.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: "/Start",
    name: "Start",
    component: Start
  },
  {
    path: '/Questions',
    name: 'Questions',
    component: Questions
  },
  {
    path: '/Results',
    name: 'Results',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Results.vue')
  }
]



const router = new VueRouter({
  routes
}) 

export default router

