let base = "https://opentdb.com/api.php?";
export function fetchData(category, difficulty, noQuestions) {
    let fullURL = base + "amount="+noQuestions;
    // https://opentdb.com/api.php?amount=10&category=10&difficulty=easy

    if (category > 0) {
        fullURL += "&category=" + category;
    }
    
    if (difficulty) {
        fullURL += "&difficulty=" + difficulty;
    }

    // console.log(fullURL);
    return fetch(fullURL)
        .then(response => response.json())
        .then(data => data.results)

}

// export function fetchCategories() {
//     return fetch("https://opentdb.com/api_category.php"+include)
//         .then(response => response.json())
//         .then(data => data.results)
// }
